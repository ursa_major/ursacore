import qbs
import "./settings.js" as Settings

Module{
	name:"UrsaDevelop.Core"
    Depends {name:"cpp"	}
    property var setiings: new Settings.settings()
    cpp.includePaths: [setiings.includePath]
}
