import qbs
import qbs.TextFile
import qbs.File

Ursacommonproject{
    type:"dynamiclibrary"
    property string srcPathFiles:""
    property string libPathRelease:""
    property string libPathDebug:""

    property string UrsacommonprojectPath: path


    Transformer{
        Artifact {
                filePath: product.srcPathFiles+"/../qbs_extensions_developers/modules/UrsaDevelop/"+product.name+"/settings.js"
                fileTags: "processed_file"
            }

            prepare:{

                var cmd = new JavaScriptCommand();
                cmd.description = "Processing qbs extensions";
                cmd.highlight = "codegen";
                cmd.sourceCode = function() {
                    var content = "function settings(){\r\n"+
                                  "this.includePath =\""+product.srcPathFiles+"\"\r\n"+
                                  "this.libraryPathDebug=\""+product.libPathDebug+"\"\r\n"+
                                  "this.libraryPathRelease=\""+product.libPathRelease+"\"\r\n"+
                                  "}"
                    file = new TextFile(output.filePath, TextFile.WriteOnly);
                    file.truncate()
                    file.write(content);
                    file.close();
                    var destPath =  product.UrsacommonprojectPath+"/../modules/UrsaDevelop/"+product.name
                    File.copy(output.filePath, destPath +"/settings.js")
                    File.copy(product.srcPathFiles+"/../qbs_extensions_developers/modules/UrsaDevelop/"+product.name+"/"+product.name+".qbs", destPath +"/"+product.name+".qbs")
                }
                return cmd;
            }
    }
}
