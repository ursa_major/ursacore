import qbs
import "./settings/additionLibraryPaths.js" as additionLibrarySettings

Product{
    Depends{name:"cpp"}
    cpp.cxxFlags:["-std=gnu++14"]
    property string EigenPath: additionLibrarySettings.eigenpath()
    property string BoostPath: additionLibrarySettings.boostPath()
    property string DevelopersExtinsionPath: path

    cpp.includePaths:[EigenPath, BoostPath]
}
