import qbs
import qbs.TextFile

Ursacommonlibraryproject{
    files: [
        "*.h",
        "*.cpp",
    ]
    name:"Core"
    Depends { name: "Qt.core" }
    srcPathFiles: path
    cpp.includePaths: [".", EigenPath, BoostPath]


    Export {
           Depends { name: "cpp" }
           Depends { name: "Qt.core" }
           cpp.includePaths: [ ".", EigenPath, BoostPath]

    }

}
