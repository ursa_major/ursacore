#pragma once
#include <UrsaGlobal.h>

namespace Ursa
{
    /**
     *Struct contain snapshot main data
     */
    template <typename T> struct Snapshot
    {
        Array2D <T> img; //! Snapshot's iamge
        long mjd; //! Snapshot's date-time in mjd year-second part
        unsigned long microseconds;
        bool isPPS; //!PPS status
    };
}
