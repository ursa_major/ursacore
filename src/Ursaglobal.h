#pragma once
#include <qglobal.h>

#define URSA_EXPORT Q_DECL_EXPORT
#include <boost/math/constants/constants.hpp>
#include <Eigen/Core>

constexpr long  double operator "" _arcsec (long double sec){return sec/3600.0/180.0*boost::math::constants::pi<long double>();}

constexpr long  double operator "" _arcdeg (long double deg){return deg/180.0*boost::math::constants::pi<long double>();}

namespace Ursa
{
    struct SphericalCoordinate{
        double longitude;
        double lattitude;
        double radius;

        inline bool operator == (const SphericalCoordinate & value) const{
            return longitude == value.longitude && lattitude == value.lattitude && radius == value.radius;
        }
    };

    constexpr double DMStoRad(double degree, double miniute, double seconds){
        return (degree + miniute/60.0 + seconds/3600.0)/180*boost::math::constants::pi<double>();
    }

    template <typename T> using Array2D = typename Eigen::Array<T, Eigen::Dynamic, Eigen::Dynamic>;
    template <typename T> using Matrix = Eigen::Matrix<T, 3, 3, Eigen::RowMajor>;

}
