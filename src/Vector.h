#pragma once
#include <UrsaGlobal.h>
#include <math.h>

namespace Ursa {

/**
 *\brief Class, based on Eigen::matrix with several usefull methods
 */
template <typename T> class Vector: public Eigen::Matrix<T, 3, 1>
{
public:
    using Eigen::Matrix<T, 3, 1>::Matrix;
    using Eigen::Matrix<T, 3, 1>::data;
    using Eigen::Matrix<T, 3, 1>::operator *;
    using Eigen::Matrix<T, 3, 1>::operator ==;

    /**
     * @brief Calculate module
     */
    T module () const
    {
        return sqrt(data()[0]*data()[0] + data()[1]*data()[1] + data()[2]*data()[2]) ;
    }

    /**
     * @brief Calculate vector product
     */
    T cross( const Vector<T> &other ) const
    {
        return data()[0]*other[0] + data()[1]*other[1] + data()[2]*other[2] ;
    }

    /**
     * @brief Calculate Sepearated angle between two vectors.
     */
    T separatedAngle(const Vector<T> other) const
    {
        double ss = cross(other)/module()/other.module();
        return -1<= ss && ss <=1? acos(ss): (ss > 0 ? 0: - boost::math::constants::pi<T>());
    }
};
}

