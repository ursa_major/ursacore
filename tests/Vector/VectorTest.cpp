#include <QtTest>
#include <Vector.h>

using namespace Ursa;
class VectorTest : public QObject
{
    Q_OBJECT

public:
    VectorTest(){}

private Q_SLOTS:
    void separated();
};



void VectorTest::separated()
{
    Vector<double> vector (1, 0, 0);

    vector(1) = sin(10);
    double separated = vector.separatedAngle(Vector<double>(0, 2, 0));

    QCOMPARE(separated, boost::math::constants::half_pi<double>());

    separated = vector.separatedAngle(Vector<double>(0, -2, 0));

    QCOMPARE(separated, boost::math::constants::half_pi<double>());

    separated = vector.separatedAngle(Vector<double>(1, 1, 0));

    QCOMPARE(separated, boost::math::constants::half_pi<double>()/2);

}

QTEST_APPLESS_MAIN(VectorTest)

#include "VectorTest.moc"

