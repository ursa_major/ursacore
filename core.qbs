import qbs


Project{
    minimumQbsVersion: "1.3.4"
    name:"UrsaCore"
    qbsSearchPaths:"qbs_extensions_developers"
    references:["src/src.qbs",
                "tests/tests.qbs"]
}
